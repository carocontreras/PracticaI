# Automatización de pruebas Elastic

La automatización de pruebas se puede dividir en dos ejes:

 1. Simulación de escritura de logs: Incluye archivos *LogCreatorDtm, LogCreatorQry, LogCreatorMSUmap, LogCreatorMSUdia, logsDia* y *logsMap.*
 2. Definición consultas: Con archivos tales como *verificarA_Dia, verificarB_Dia, verificarA_Map* y *verificarB_Map.*

Todos estos archivos fueron desarrollados en *Python 2.7*.

## Simulación de escritura de logs

Esta etapa comprende la creación de logs ficticios de 4 tipos, configurados según los archivos de configuración de logstash.

### 1. *qry:*

Posee la siguiente estructura

```text
%{WORD:binaryname},%{TIMEBRIDGE:logtime},%{WORD:plname},%{DATA:callid},%{PROTOID:protoid},%{IMEI:imei},%{STATUS:imeistatus},%{ERRORQRY:error}
```

donde:

   - ***binaryname***: Constante para todos los logs. *bridge_4*.
   - ***logtime/date***: Fecha actual con formato YYYYMMDD|HHMISS.mls, por ejemplo, *20180226|111802.501*.
   - ***plName***: String constante. *check_imei_db*
   - ***callId***: Número de 19 dígitos creados al azar, por ejemplo, *1101660418249091440*.
   - ***protoid***: Número al azar entre [5, 7].
   - ***imei***: Número de 14 dígitos al azar, por ejemplo, *16596759182465*.
   - ***imeistatus/status***: Número entre [-1, 2]
   - ***error***: Número entre [-1, 2]

Lo cual en la práctica queda como:

```text
bridge_4,20180226|111802.501,check_imei_db,1101660418249091440,7,16596759182465,2,4
```

Para realizar esto se utiliza el programa *LogCreatorQry.py*. Este programa crea los logs según las configuraciones antes destacadas y los guarda en files bajo el nombre *LogQry_fecha.qry* con *fecha* teniendo el formato YYYYMMDDHHMISS. Se almacenan en */opt/sixlabs/var/log* o en algún directorio proporcionado por el usuario.

### 2. *dtm:*

Con estructura:

```text
%{WORD},%{TIMEBRIDGE:logtime},%{WORD},%{WORD},%{DATA},%{NONNEGINT:sigastime},%{NONNEGINT:dbtime}$
```

donde:

   - ***binaryname***: Constante para todos los logs. *bridge*.
   - ***logtime/date***: Fecha actual con formato YYYYMMDD|HHMISS.mls, por ejemplo, *20180226|121127.936*.
   - ***threadId***: String *thr* más 3 números al azar, por ejemplo, *thr970*
   - ***plName***: String constante. *check_imei_db*
   - ***callId***: Número de 19 dígitos creados al azar, por ejemplo, *4334605079679926454*.
   - ***sigastime***: Número al azar de 3 dígitos, por ejemplo, *030*.
   - ***dbtime***: Número al azar de 3 dígitos, por ejemplo, *870*.

Lo cual se expresa como:

```text
bridge,20180226|121127.936,thr970,check_imei_db,4334605079679926454,030,870
```

El programa utilizado para desarrollar estos logs es *LogCreatorDtm.py*, funcionando de forma análoga a como lo hace *LogCreatorQry*, con la diferencia de que los logs aquí creados se gurdan en archivos de nombre *logdtm_fecha.dtm* con *fecha* con el mismo formato que para los qry.

### 3. *MSUdia:*

Cumpliendo la estructura:

```text
324,%{ZEROPOSINT:received},%{ZEROPOSINT:sended}$
```

con:

   - ***324***: Constante para todos los logs.
   - ***received***: Número entero al azar con lardo entre [1,9], por ejemplo, *7756069*.
   - ***sended***: Número entero al azar con lardo entre [1,9], por ejemplo, *538967284*.

Quedando como:

```text
324,7756069,538967284
```

Con funcionalidad análoga a *LogCreatorQry*, *LogcreatorMSUdia.py* crea este tipo de logs, los cuales son guardados en *DIA_1-MSU-fecha.csv*. La particularidad en este programa es que solo se guarda un log por archivo.

### 3. *MSUmap:*

Bajo la estructura:

```text
M3UA,%{ZEROPOSINT},%{ZEROPOSINT},%{ZEROPOSINT},%{ZEROPOSINT:received},%{ZEROPOSINT:sended}$
```

Tal que comienza con una constante *"M3UA"* que luego es seguido por 4 números enteros al azar de entre 1 y 9 dígitos. En particular, a los dos últimos ***received*** y ***sended***. Un ejemplo de esto sería:

```text
M3UA,77322,2085,77322,5343397,5
```

Este proceso se hace con *LogcreatorMSUmap.py*, el cual, al igual que los creadores anteriores, en terminos generales funciona de igual forma que *LogCreatorQry*, aunque aquí cada 2 logs se guardan en files con nombre *SIG-MSU-fecha.csv*.

&nbsp;

Usando estos creadores es que se llega a los programas dos programas principales, los cuales se encargan de ejecutarlos en conjunto. Estos programas son:

   - ***logsDia.py*** : Ejecuta *LogCreatorDtm.py*, *LogCreatorQry.py* y *LogcreatorMSUdia.py*.
   - ***logsMap.py*** : Ejecuta *LogCreatorDtm.py*, *LogCreatorQry.py* y *LogcreatorMSUmap.py*

## Definición consultas

En esta fase se verifica si cada log fue subido a Elastic en el index correspondiente, diferenciando entre sitio A y sitio B, además de comprobaciones de tipo DIA y MAP en cada uno de ellos.

En primer lugar se verifica la conexión a elastic y kibana a través de la librería *request*. Luego se establece la conexión con elastic ocupando la librería *elasticsearch*

### Diferencia entre sitio A y B
Radica en el puerto donde se conectan ElasticSearch y Kibana. Para el sitio A:

```python
res = requests.get('http://10.98.119.90:9200')
kib = requests.get('http://10.98.119.90:5601')
```

Para el sitio B:

```python
res = requests.get('http://10.98.129.90:9202')
kib = requests.get('http://10.98.129.90:5601')
```
