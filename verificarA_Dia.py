import requests
from elasticsearch import *
import json
import glob, os
import datetime

#Elimina salto de linea (/n) de una palabra
def sacarN(palabra):
    newPal=""
    for i in range(0,len(palabra)-1):
        newPal= newPal + palabra[i]
    return newPal

#Entrega fecha con formato YYYY.MM.DD
def fecha():
        fecha = datetime.datetime.now()
        dia = str(fecha.day)
        mes = str(fecha.month)
        anio = str(fecha.year)

        #Agrega 0s
        if len(dia) ==1:
            dia = "0" + dia

        if len(mes) ==1:
            mes = "0" + mes

        return ""+anio+"."+mes+"."+dia


#Entrega fecha con formato YYYYMMDD
def fechatitulo():
        fecha = datetime.datetime.now()
        dia = str(fecha.day)
        mes = str(fecha.month)
        anio = str(fecha.year)

        #Agrega 0s
        if len(dia) ==1:
            dia = "0" + dia

        if len(mes) ==1:
            mes = "0" + mes

        return ""+anio+""+mes+""+dia



# make sure ES and Kibana are up and running in site A
res = requests.get('http://10.98.119.90:9200')
kib = requests.get('http://10.98.119.90:5601')

#Conexion a elastic
es = Elasticsearch([{'host': '10.98.119.90', 'port': 9200}])

#Busca en elastic un index que comience con archivo
#En caso de encontrarlo retorna 0, si no sigue buscando iterativamente
#Si tal indice no existe retorna 1 y un mensaje avisando de la no existencia de el
def allIndex(elastic, archivo):
  salida = 0
  for index in elastic.indices.get('*'):
   if fecha() in index: #da la condicion de que sean de la fecha actual
    if not index.startswith(archivo):
      pass

    else:
      return 0
  print("Falta index "+ archivo)
  return 1


#Toma un log de dtm y revisa si existe su dbtime y su sigastime en un indice ind, 
#creando un archivo con el resultado
#ej: revisaDtm(log, 'dtm-2018.02.13-a')
#revisaDtm("bridge,20180213|132226.762,thr243,check_imei_db,3070424778620183333,124,401", 'dtm-2018.02.13-a' )
def revisaDtm(log, ind):
    partes = log.split(",")
    sigastime = partes[5] #sigastime en el log
    dbtime = sacarN(partes[6]) #dbtime en el log

    consulta=(es.search(index=ind, body={
        "query": {
            "bool": {
            "must": [ #consulta por lo que debe estar
                {"term": {"dbtime": dbtime} },
                {"term": {"sigastime": sigastime}}
            ]
            }
        },
        "size": 1000 #hasta 1000 resultados
    }))

    #Guarda resultado de la consulta en un archivo temporal llamado logDtm.log
    with open("/reportes-test/logCreators/logDtm.log", 'w') as file:
        json.dump(consulta,file, indent=2)

#Toma un log de qry y revisa si sus fields existen en el indice ind, 
#creando un archivo con el resultado
#ej: revisaQry(log, 'qry-2018.02.13-a')
def revisaQry(log, ind):
    partes = log.split(",")
    binaryname = partes[0]
    imei = partes[5]
    protoidcode = partes[4]
    plname = partes[2]
    callid = partes[3]

    consulta=(es.search(index=ind, body={
        "query": {
            "bool": {
            "must": [
                {"term": {"binaryname": binaryname} },
                {"term": {"imei": imei}},
                {"term": {"protoidcode": protoidcode}},
                {"term": {"plname": plname}},
                {"term": {"callid": callid}}
            ]
            }
        },
        "size": 1000
    }))

    #Guarda resultado de la consulta en un archivo temporal llamado logDtm.log
    with open("/reportes-test/logCreators/logQry.log", 'w') as file:
        json.dump(consulta,file, indent=2) 

#Revisa si para un log en particular existe msusource.keyword, received y sended dentro del indice, 
#creando un archivo con el resultado
#ej: revisaMsu(log, 'msu-2018.02.13-a')
def revisaMsu(log, ind):
    partes = log.split(",")
    l = len(partes)
    consulta = 0
    msusource = partes[0]

    if msusource=="324": #ignora los MAP
         msusource = "DIA"

         sended = sacarN(partes[l-1])

         received = partes[l-2]

         consulta=(es.search(index=ind, body={
           "query": {
            "bool": {
            "must": [
                {"term": {"sended": sended}},
                {"term": {"received": received}},
                {"term": {"msusource.keyword": msusource}}
            ]
            }
           },
          "size": 1000
         }))


    #Guarda resultado de la consulta en un archivo temporal llamado logDtm.log
    with open("/reportes-test/logCreators/logMsu.log", 'w') as file:
        json.dump(consulta,file, indent=2)


#Retorna los hits obtenidos en un query desde el achivo .log donde estan guardados los revisa*
#Retorna 0 si no existe y >0 si existe
#revisaDtm("bridge,20180213|132226.762,thr243,check_imei_db,3070424778620183333,124,401", 'dtm-2018.02.13-a' )
def getHit(path):
    f= open(path, 'r')
    for line in f.readlines():
        if "total" in line:
            partes = line.split(":")
            coma = partes[1].split(" ")
            num = coma[1].split(",")
            hits = num[0]
            return int(hits)

    return -1 #En caso de error entrega -1

# Revisa si todos los logs de los files .dtm que se encuentran en el pathLogs estan dentro del indice ind
# Si el log esta perdido se deja de analizar el file y se imprime la linea perdida
# Si se encuentra el log se sigue analizando el file hasta el termino de lineas
# allDtm('/opt/sixlabs/var/log', 'dtm-2018.02.13-a')
def allDtm(pathLogs, ind):
    os.chdir(pathLogs) #Cambio directorio a pathLogs
    for file in glob.glob("*.dtm"): #Analiza todos los files con extension .dtm
        if (fechatitulo() in file): #Si son files del dia
          f=open(file,'r')
          for line in f:
            revisaDtm(line,ind) #Analizo si line esta en el indice ind

            if getHit("/reportes-test/logCreators/logDtm.log")<=0: #Si no hay hit o hay error
                print("Log perdido en index dtm: " + line)
                break #no seguir analizando ese file
          print("file: " + file + " listo")
    os.remove("/reportes-test/logCreators/logDtm.log") #eliminar el archivo temporal de las consultas

# Revisa si todos los logs de los files .qry que se encuentran en el pathLogs estan dentro del indice ind
# Si el log esta perdido se deja de analizar el file y se imprime la linea perdida
# Si se encuentra el log se sigue analizando el file hasta el termino de lineas
#allQry('/opt/sixlabs/var/log', 'qry-2018.02.13-a')
def allQry(pathLogs, ind):
    os.chdir(pathLogs) #Cambio directorio a pathLogs
    for file in glob.glob("*.qry"): #Analiza todods los files con extension .qry
        if (fechatitulo() in file): #Si son files del dia
          f=open(file,'r')
          for line in f:
            revisaQry(line,ind) #Analizo si line esta en el indice ind      

            if getHit("/reportes-test/logCreators/logQry.log")<=0: #Si no hay hit o hay error
                print("Log perdido en index qry: "+ line)
                break
          print("file: "+ file + " listo")
    os.remove("/reportes-test/logCreators/logQry.log") #eliminar archivo temporal de las consultas


# Revisa si todos los logs de los files msu*.csv que se encuentran en el pathLogs estan dentro del indice ind
# Si el log esta perdido se deja de analizar el file y se imprime la linea perdida
# Si se encuentra el log se sigue analizando el file hasta el termino de lineas
# allMsu('/opt/sixlabs/var/log', 'msu-2018.02.13-a')
def allMsu(pathLogs, ind):
    os.chdir(pathLogs) #Cambio directorio a pathLogs
    for file in glob.glob("DIA*.csv"): #Ignora los logs de MAP
        if (fechatitulo() in file): #Si son files del dia actual
          f=open(file,'r')
          for line in f:
            revisaMsu(line,ind) #creo file para analizar

            if getHit("/reportes-test/logCreators/logMsu.log")<=0: #Si no hay hit o hay error
                print("Log perdido en index msu: "+ line)
                break
          print("file: " + file + " listo")
    os.remove("/reportes-test/logCreators/logMsu.log") #Eliminar archivo temporal de las consultas



def main():
    path= raw_input("ruta de los logs? (default: /opt/sixlabs/var/log:)") #Ruta donde se encuentran los files de logs
    
    if (len(path)==0): #Si se escoge el path por default
        if (allIndex(es,'msu')==0): #si msu existe
          allMsu('/opt/sixlabs/var/log', 'msu-'+ fecha()+ '-a')
        if (allIndex(es, 'qry')==0): #si qry existe
          allQry('/opt/sixlabs/var/log', 'qry-'+ fecha()+ '-a')
        if (allIndex(es, 'dtm')==0): #si dtm existe
          allDtm('/opt/sixlabs/var/log', 'dtm-'+ fecha()+'-a')

        else: #si no existen indices dtm, msu o qry
          print("No hay index")
    
    else: #Si se recibe un path especifico
        if (allIndex(es,'msu')==0): #si msu existe
          allMsu(path, 'msu-'+ fecha()+ '-a')
        if (allIndex(es, 'qry')==0): #si qry existe
          allQry(path, 'qry-'+ fecha()+ '-a')
        if (allIndex(es, 'dtm')==0): #si dtm existe
          allDtm(path, 'dtm-'+ fecha()+'-a')

        else: #si no existen indices dtm, msu o qry
          print("No hay index")


if __name__ == '__main__':
    main()
    print("Verificacion terminada")

